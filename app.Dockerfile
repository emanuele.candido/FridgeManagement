# Current latest python version
FROM python:3.9.5

# Default of Flask
EXPOSE 5000

# Install requirements
COPY WebApp/ /app/web_app/