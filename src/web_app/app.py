from flask import Flask, render_template, url_for
app = Flask(__name__)

my_recipes = [
    {
        "title":"Pizza",
        "author":"John"
    },
    {
        "title":"Roasted Potatoes",
        "author":"Jane"
    } 
]

@app.route('/')
@app.route('/welcome')
def welcome():
    return render_template('welcome.html')

@app.route('/recipes')
def recipes():
    return render_template('recipes.html', my_recipes=my_recipes, title="recipes")

if __name__ == '__main__':
    app.run()
