DROP TABLE IF EXISTS item;

CREATE TABLE IF NOT EXISTS item (
    shopping_list   INT UNSIGNED    NOT NULL,
    food_name       CHAR(30)        NOT NULL,
    quantity        FLOAT           NOT NULL,
    bought          FLOAT           NOT NULL    DEFAULT 0,
    PRIMARY KEY (shopping_list, food_name),
    FOREIGN KEY (food_name)     REFERENCES  food (food_name),
    FOREIGN KEY (shopping_list) REFERENCES  shopping_list (id_shopping_list)
)ENGINE = InnoDB;
