DROP TABLE IF EXISTS course;

CREATE TABLE IF NOT EXISTS course (
    id_course   INT UNSIGNED    NOT NULL    AUTO_INCREMENT,
    meal        CHAR(30)	    NOT NULL,
    meal_date   DATE	        NOT NULL,
    title       CHAR(30)	    NOT NULL,
    PRIMARY KEY (id_course),
    FOREIGN KEY (meal, meal_date)   REFERENCES  meal_plan (meal, meal_date),
    FOREIGN KEY (title)             REFERENCES  recipe (title)
)ENGINE = InnoDB;
