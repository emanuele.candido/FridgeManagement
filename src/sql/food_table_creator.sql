DROP TABLE IF EXISTS food;

CREATE TABLE IF NOT EXISTS food (
    food_name       CHAR(30)    NOT NULL,
    measure_unit    CHAR(30)    NOT NULL,
    PRIMARY KEY (food_name)
);
