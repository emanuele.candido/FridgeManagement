DROP TABLE IF EXISTS shopping_list;

CREATE TABLE IF NOT EXISTS shopping_list (
    id_shopping_list    INT UNSIGNED    NOT NULL    AUTO_INCREMENT,
    shopping_date       DATE,
    planned_date        DATE            NOT NULL,
    PRIMARY KEY (id_shopping_list)
);
