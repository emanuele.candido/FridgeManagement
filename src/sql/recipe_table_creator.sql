DROP TABLE IF EXISTS recipe;

CREATE TABLE IF NOT EXISTS recipe (
    title           CHAR(30)    NOT NULL,
    instructions    TEXT        NOT NULL,
    people          INT         NOT NULL,
    PRIMARY KEY (title)
);
