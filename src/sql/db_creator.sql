DROP DATABASE IF EXISTS fridge_manager;

CREATE DATABASE IF NOT EXISTS fridge_manager
    CHARACTER SET = utf8mb4;
	
USE fridge_manager;
