DROP TABLE IF EXISTS supply;

CREATE TABLE IF NOT EXISTS supply (
    expiration_date DATE        NOT NULL,
    food_name       CHAR(30)    NOT NULL,
    quantity        FLOAT       NOT NULL,
    reserved        FLOAT       NOT NULL    DEFAULT 0,
    PRIMARY KEY (expiration_date, food_name),
    FOREIGN KEY (food_name) REFERENCES  food (food_name)
)ENGINE = InnoDB;
