DROP TABLE IF EXISTS meal_plan;

CREATE TABLE IF NOT EXISTS meal_plan (
    meal            CHAR(30)    NOT NULL,
    meal_date       DATE        NOT NULL,
    participants    INT         NOT NULL,
    PRIMARY KEY (meal, meal_date)
);
