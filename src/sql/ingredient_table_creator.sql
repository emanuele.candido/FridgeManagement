DROP TABLE IF EXISTS ingredient;

CREATE TABLE IF NOT EXISTS ingredient (
    recipe      CHAR (30)   NOT NULL,
    ingredient  CHAR (30)   NOT NULL,
    quantity    FLOAT       NOT NULL,
    PRIMARY KEY (recipe, ingredient),
    FOREIGN KEY (recipe)        REFERENCES  recipe (title),
    FOREIGN KEY (ingredient)    REFERENCES  food (food_name)
)ENGINE = InnoDB;
