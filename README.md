# FridgeManagement
A simple tool to manage your fridge stock

Based on MySQL allows, through a web interface, the following:
- insert a list of foods
- insert a list of recipes
- plan your meals for next week and beyond
- generate automaticly a shopping list based on your planned meals and on your fridge stocks
- signaling if there are expiring stocks
- insert automatically fridge stocks based on a shopping list
